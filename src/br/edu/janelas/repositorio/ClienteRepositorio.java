package br.edu.janelas.repositorio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import br.edu.janelas.modelo.Cliente;

public class ClienteRepositorio implements Repositorio<Cliente> {

	@Override
	public int create(Cliente e) {
		int res=0;

		try {
			Connection conn = Conexao.getConexao();
			PreparedStatement ps = conn.prepareStatement("insert into cliente values (?,?)");
			ps.setString(1, e.getCodigo());
			ps.setString(2, e.getNome());
			res = ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return res;
	}

	@Override
	public Vector<Cliente> readAll() {
		Vector<Cliente> clientes= new Vector<Cliente>();

		try {
			Connection conn = Conexao.getConexao();
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery("select * from cliente");
			while (rs.next()) {
				Cliente temp = new Cliente();
				temp.setCodigo(rs.getString(1));
				temp.setNome(rs.getString(2));
				clientes.add(temp);
			}
			stm.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		return clientes;
	}

	@Override
	public Cliente read(Cliente e) {
		Cliente temp = null;
		try {
			Connection conn = Conexao.getConexao();
			PreparedStatement ps = conn.prepareStatement("select * from cliente where codigo=?");
			ps.setString(1, e.getCodigo());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				temp = new Cliente();
				temp.setCodigo(rs.getString(1));
				temp.setNome(rs.getString(2));
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return temp;
	}

	@Override
	public int update(Cliente e) {
		int res=0;
		try {
			Connection conn = Conexao.getConexao();
			PreparedStatement ps = conn.prepareStatement("update cliente set nome=? where codigo=?");
			ps.setString(1, e.getNome());
			ps.setString(2, e.getCodigo());
			res = ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return res;
	}

	@Override
	public int delete(Cliente e) {
		int res=0;
		try {
			Connection conn = Conexao.getConexao();
			PreparedStatement ps = conn.prepareStatement("delete from cliente where codigo=?");	
			ps.setString(1, e.getCodigo());
			res = ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}		
		return res;
	}



}
