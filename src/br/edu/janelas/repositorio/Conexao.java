package br.edu.janelas.repositorio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

class Conexao {

	public static Connection getConexao() throws SQLException {
		return DriverManager.getConnection("jdbc:postgresql://localhost:5432/clientes","postgres","postgres");
	}
	
}
