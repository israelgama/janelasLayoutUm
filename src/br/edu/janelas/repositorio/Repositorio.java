package br.edu.janelas.repositorio;

import java.util.Vector;

public interface Repositorio<T> {

	public int create(T e);
	public Vector<T> readAll();
	public T read(T e);
	public int update(T e);
	public int delete(T e);
	
}
